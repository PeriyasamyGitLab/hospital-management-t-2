package com.cbt.springjwt.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cbt.springjwt.exception.ResourceNotFoundException;
import com.cbt.springjwt.models.AppointmentEntity;
import com.cbt.springjwt.repository.AppointmentRepository;

@Service
public class AppointmentService {
	@Autowired
	AppointmentRepository appointmentRepository;
	
	public AppointmentEntity saveAppointment(AppointmentEntity data) {
		return this.appointmentRepository.save(data);
	}
	public List<AppointmentEntity> getAppointment()
	{
		return this.appointmentRepository.findAll();
	}
	 
	
	public AppointmentEntity updateFromDoctor(Integer id,AppointmentEntity value) {
		AppointmentEntity update=this.appointmentRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Id Is Incorrect"));
		update.setTime(value.getTime());
		update.setTime(value.getTime());
		return this.appointmentRepository.save(update);
		
	}
	public Optional<AppointmentEntity> getSingleData(Integer id){
		return this.appointmentRepository.findById(id);
	}
}
