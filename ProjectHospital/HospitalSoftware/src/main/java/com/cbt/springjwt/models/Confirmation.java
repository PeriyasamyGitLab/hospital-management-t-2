package com.cbt.springjwt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;


@Entity
@Table(name = "confirmation")
	  	

public class Confirmation {



   @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String patientName;
    private String time;
    
    private String message;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Confirmation(int id, String patientName, String time, String message) {
		super();
		this.id = id;
		this.patientName = patientName;
		this.time = time;
		this.message = message;
	}

	public Confirmation() {
		super();
		// TODO Auto-generated constructor stub
	}
   
    



//   public Confirmation() {
//	   super();
//   }
//    
//    public Confirmation(Integer id, String time, String message) {
//        super();
//        this.id = id;
//       this.time = time;
//        this.message = message;
//    }
//
//
//
//   public Integer getId() {
//        return id;
//    }
//
//
//
//   public void setId(Integer id) {
//        this.id = id;
//    }
//
//
//
//   public String getTime() {
//        return time;
//    }
//
//
//
//   public void setTime(String time) {
//        this.time = time;
//    }
//
//
//
//   public String getMessage() {
//        return message;
//    }
//
//
//
//   public void setMessage(String message) {
//        this.message = message;
//    }
//    
}