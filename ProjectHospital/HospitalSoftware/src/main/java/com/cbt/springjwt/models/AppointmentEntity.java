package com.cbt.springjwt.models;


import java.sql.Date;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



@Entity
@Table(name="appointmenttable" , uniqueConstraints = { 
	      @UniqueConstraint(columnNames = "patientName"),
	
	    })
public class AppointmentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String patientName;
	private int age;
	private Date date;
	private String time;
	private String gender;
	private String problems;
	
//	  @ManyToMany(fetch = FetchType.LAZY)
//	  @JoinTable(  name = "appointment-conformation", 
//	        joinColumns = @JoinColumn(name = "appointment_id"), 
//	        inverseJoinColumns = @JoinColumn(name = "confirmation_id"))
//	  private Set<Confirmation> confirmation = new HashSet<>();
	
	public AppointmentEntity() {
		super();
		
	}
	public AppointmentEntity(int id, String patientName, int age, Date date,String time, String gender,
			String problems) {
		super();
		this.id = id;
		this.patientName = patientName;
		this.age = age;
		this.date = date;
		this.time = time;
		this.gender = gender;
		this.problems = problems;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getProblems() {
		return problems;
	}
	public void setProblems(String problems) {
		this.problems = problems;
	}


}
