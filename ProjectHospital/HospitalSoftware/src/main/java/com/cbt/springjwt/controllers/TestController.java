package com.cbt.springjwt.controllers;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cbt.springjwt.models.User;
import com.cbt.springjwt.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	@Autowired
	UserRepository userrepositroy;
	
  @GetMapping("/all")
  public String allAccess() {
    return "Public Content.";
  }





  @GetMapping("/admin")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<User> adminAccess() {
    return  this.userrepositroy.findAll();
  }
  
  @DeleteMapping("/admin/{id}")
  public void delete(@PathVariable (value="id")Long id) {
	  this.userrepositroy.deleteById(id);
  }
  
  @GetMapping("/doc")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public List<User> docdetails(){
	  return this.userrepositroy.findAll();
  }
}
