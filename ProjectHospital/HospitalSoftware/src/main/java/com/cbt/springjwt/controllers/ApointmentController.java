package com.cbt.springjwt.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cbt.springjwt.models.AppointmentEntity;
import com.cbt.springjwt.security.services.AppointmentService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class ApointmentController {
	@Autowired
	AppointmentService appointmentService;
	
	@PostMapping("/user")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public AppointmentEntity appointmentData(@RequestBody AppointmentEntity data) {
		return this.appointmentService.saveAppointment(data);
	}
	@GetMapping("/mod/get")
	@PreAuthorize("hasRole('ROLE_MODERATOR') ")
	public List<AppointmentEntity> getAppointmentData(){
		return this.appointmentService.getAppointment();
	}
	
//	
//	@GetMapping("getStaffById/{id}")
//    @PreAuthorize("hasRole('ADMIN')")
//    public Optional<Staff>getById(@PathVariable(value = "id") Long id){
//        return this.staffService.getByid(id);
//    }
	
	
	
	//PUt
	@PutMapping("/mod/{id}")
	@PreAuthorize("hasRole('ROLE_MODERATOR') ")
	public AppointmentEntity updatedDoctorData(@PathVariable (value="id")Integer id,@RequestBody AppointmentEntity value ) {
		return this.appointmentService.updateFromDoctor(id,value);
	}
	//getSingleData
	@GetMapping("/doc/{id}")
	@PreAuthorize("hasRole('ROLE_MODERATOR') ")
	public Optional<AppointmentEntity> getSingleData(@PathVariable(value="id") Integer id){
		return this.appointmentService.getSingleData(id);
	}
	
	
	

}
