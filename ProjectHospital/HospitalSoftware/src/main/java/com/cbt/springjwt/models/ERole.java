package com.cbt.springjwt.models;

public enum ERole {
//  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}
