package com.cbt.springjwt.security.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cbt.springjwt.models.Confirmation;
import com.cbt.springjwt.repository.ConfirmationRepository;

@Service
public class ConfirmationService {
@Autowired
ConfirmationRepository confirmationRepository;
	 
	public Confirmation messagePost(Confirmation data) {
		return  this.confirmationRepository.save(data);
	}
	public List<Confirmation> messageGet(){
		return this.confirmationRepository.findAll();
	}
}
