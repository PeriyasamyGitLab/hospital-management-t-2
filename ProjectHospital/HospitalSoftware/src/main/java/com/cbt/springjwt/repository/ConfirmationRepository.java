package com.cbt.springjwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cbt.springjwt.models.Confirmation;

public interface ConfirmationRepository extends JpaRepository<Confirmation,Integer>{

}
