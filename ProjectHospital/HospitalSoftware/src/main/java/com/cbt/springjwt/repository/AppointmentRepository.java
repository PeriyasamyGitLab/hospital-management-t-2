package com.cbt.springjwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cbt.springjwt.models.AppointmentEntity;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, Integer> {

}
