package com.cbt.springjwt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cbt.springjwt.models.Confirmation;
import com.cbt.springjwt.security.services.ConfirmationService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class ConfirmationController {
	@Autowired
	 ConfirmationService confirmationService;
	
	@PostMapping("mod/msg")
	@PreAuthorize("hasRole('ROLE_MODERATOR')")
	public Confirmation postMessage(@RequestBody Confirmation message) {
		return this.confirmationService.messagePost(message);
	}
	
	@GetMapping("mod/msg")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Confirmation> getMessage(){
		return this.confirmationService.messageGet();
	}
}
