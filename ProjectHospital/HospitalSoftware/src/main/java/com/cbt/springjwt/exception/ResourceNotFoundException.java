package com.cbt.springjwt.exception;

public class ResourceNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 21L;

	public ResourceNotFoundException(String error) {
		super(error);
	}

}
