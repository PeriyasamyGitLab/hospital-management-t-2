import { Time } from '@angular/common';

export class MessageService {
  id!: number;
  message!: String;
  time!: Time;
  patientName!:String;
}
