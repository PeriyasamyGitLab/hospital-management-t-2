import { Time } from '@angular/common';

export class AppoinmentData {
  id!: number;
  patientName!: String;
  age!: number;
  date!: Date;
  time!: Time;
  gender!: String;
  problems!: String;
}
