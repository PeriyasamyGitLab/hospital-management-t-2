import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppoinmentData } from './Service/AppoinmentData';
import { MessageService } from './Service/MessageService';
import { Employee } from '../employee';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // delete(id: number) {
  //   throw new Error('Method not implemented.');
  // }
  constructor(private http: HttpClient) {}

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin');
  }

  //Appointment
  //Post
  postAppointment(value: AppoinmentData): Observable<Object> {
    return this.http.post(API_URL + 'user', value);
  }
  //Get All Data
  getAppointmentDataAll(): Observable<AppoinmentData[]> {
    return this.http.get<AppoinmentData[]>(API_URL + 'mod/get');
  }

  //update Time
  updateData(id: number, value: AppoinmentData): Observable<Object> {
    return this.http.put<Object>(API_URL + 'mod/' + id, value);
  }
  //getSingleData
  getSingleData(id: number): Observable<Object> {
    return this.http.get<Object>(API_URL + 'doc/' + id);
  }
  //Post Message
  postMessage(value: MessageService): Observable<Object> {
    return this.http.post(API_URL + 'mod/msg', value);
  }
  //get message
  getMessage(): Observable<MessageService[]> {
    return this.http.get<MessageService[]>(API_URL + 'mod/msg');
  }

  //for doctor details
  getDoctorDetails(): Observable<Employee[]> {
    return this.http.get<Employee[]>(API_URL + 'doc');
  }
  //Delete Doctor
  delete(id: number): Observable<Object> {
    return this.http.delete(API_URL + 'admin/' + id);
  }
}
