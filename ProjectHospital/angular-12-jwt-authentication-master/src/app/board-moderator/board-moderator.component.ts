import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppoinmentData } from '../_services/Service/AppoinmentData';
import { MessageService } from '../_services/Service/MessageService';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css'],
})
export class BoardModeratorComponent implements OnInit {
  appointmentData: AppoinmentData[] = [];
  conformMessage: MessageService = new MessageService();
  messageConfirm: String = 'Appointment Is Conformed';
  messageDecline: String = 'Appointment Is Cancelled';

  constructor(private userService: UserService, private router: Router,private toast:ToastrService) {}

  ngOnInit(): void {
    this.getAppointmentDataAll();
  }

  //All Data
  getAppointmentDataAll() {
    this.userService.getAppointmentDataAll().subscribe((data) => {
      this.appointmentData = data;
      console.log(data);
    });
  }
  //Update Time
  updateTime(id: number) {
    this.router.navigate(['/update', id]);
  }

  //pass Confirm Message
  conform(time: any, name: String) {
    this.conformMessage.message = this.messageConfirm;
    this.conformMessage.time = time;
    this.conformMessage.patientName = name;
    this.userService.postMessage(this.conformMessage).subscribe((data) => {
      console.log(data);
      // alert('Appointment Sent');
      this.toast.success("Appointment Sent");
    });
  }

  //pass Declined Message
  decline(time: any, name: String) {
    this.conformMessage.message = this.messageDecline;
    this.conformMessage.time = time;
    this.conformMessage.patientName = name;
    this.userService.postMessage(this.conformMessage).subscribe((data) => {
      console.log(data);
      // alert('Appointment Cancelled');
      this.toast.error("Appointment Cancelled");
    });
  }
}
