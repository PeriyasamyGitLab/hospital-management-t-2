import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AppoinmentData } from '../_services/Service/AppoinmentData';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css'],
})
export class BoardUserComponent implements OnInit {
  appointment: AppoinmentData = new AppoinmentData();
  constructor(private userService: UserService, private router: Router,private toast:ToastrService) {}

  ngOnInit(): void {}

  onSubmit() {
    this.userService.postAppointment(this.appointment).subscribe((data) => {
      console.log(data);
      // alert('Successfully Registered');
      this.toast.success("Appointment Success..!");
    });
  }
  nav() {
    this.router.navigate(['/userconformation']);
  }
  adminNavigation() {
    this.router.navigate(['admin']);
  }
}
