import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css'],
})
export class BoardAdminComponent implements OnInit {
  employees: Employee[] = [];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}
  report() {
    this.router.navigate(['/userconformation']);
  }

  appointment() {
    this.router.navigate(['/user']);
  }
  adddoctor() {
    this.router.navigate(['/register']);
  }

  docdetails() {
    this.router.navigate(['/doctoravailability']);
  }
}
