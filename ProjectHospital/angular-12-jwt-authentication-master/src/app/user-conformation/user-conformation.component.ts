import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../_services/Service/MessageService';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-user-conformation',
  templateUrl: './user-conformation.component.html',
  styleUrls: ['./user-conformation.component.css'],
})
export class UserConformationComponent implements OnInit {
  message: MessageService[] = [];
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.userService.getMessage().subscribe((data) => {
      this.message = data;
      console.log(data);
    });
  }
  adminNavigation() {
    this.router.navigate(['admin']);
  }
}
