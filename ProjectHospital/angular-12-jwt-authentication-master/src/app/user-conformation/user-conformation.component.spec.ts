import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserConformationComponent } from './user-conformation.component';

describe('UserConformationComponent', () => {
  let component: UserConformationComponent;
  let fixture: ComponentFixture<UserConformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserConformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserConformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
