import { getAllLifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Employee } from '../employee';
import { EmployeeDoctor } from '../EmployeeDoctor';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-doctor-availability',
  templateUrl: './doctor-availability.component.html',
  styleUrls: ['./doctor-availability.component.css'],
})
export class DoctorAvailabilityComponent implements OnInit {
  data: Employee[] = [];

  constructor(private userService: UserService, private router: Router,private toast:ToastrService) {}

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.userService.getDoctorDetails().subscribe((data) => {
      for(let i=0; i<data.length;i++){
        if(data[i].roles[0].id!=1){
        this.data.push(data[i]);
      }
    }
      console.log(data);
      
    });
  }

  onEdit(id: number) {
    this.router.navigate(['/doctorupdate', id]);
  }

  onDelete(id: number) {
    this.userService.delete(id).subscribe((pass) => {
      this.data=[];
      this.ngOnInit();
      this.toast.error("Deleted Successfully");
    });
  }
}
