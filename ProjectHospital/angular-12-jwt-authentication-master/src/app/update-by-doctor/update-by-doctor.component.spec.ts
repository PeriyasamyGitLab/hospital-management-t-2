import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateByDoctorComponent } from './update-by-doctor.component';

describe('UpdateByDoctorComponent', () => {
  let component: UpdateByDoctorComponent;
  let fixture: ComponentFixture<UpdateByDoctorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateByDoctorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateByDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
