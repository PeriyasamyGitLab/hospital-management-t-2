import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppoinmentData } from '../_services/Service/AppoinmentData';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-update-by-doctor',
  templateUrl: './update-by-doctor.component.html',
  styleUrls: ['./update-by-doctor.component.css'],
})
export class UpdateByDoctorComponent implements OnInit {
  appointmentUpdateData: AppoinmentData = new AppoinmentData();
  singleData: any;
  id!: number;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private toast:ToastrService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.userService.getSingleData(this.id).subscribe((data) => {
      this.singleData = data;
      console.log(data);
    });
  }
  // get Single Data

  onSubmit() {
    this.appointmentUpdateData.time = this.singleData.time;
    this.userService
      .updateData(this.id, this.appointmentUpdateData)
      .subscribe((data) => {
        console.log(data);
        this.nav();
        this.toast.success("Update success");
      });
  }
  nav() {
    this.router.navigate(['mod']);
  }
}
